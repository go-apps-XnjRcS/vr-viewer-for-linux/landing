import {animateCSS} from "./blocks/animate-css-helper";
import $ from 'jquery'
import scrollToElement from 'scroll-to-element'
import 'jquery-viewport-checker'
import getLastInFolder from "./blocks/get-last-in-album";
import disqus from "./blocks/disqus";

/*----------------------------------
Menu Toggler
----------------------------------*/
function MenuToggler(){
	let navbarToggler = document.querySelector("#navbar-toggler"),
		mainMenuEls   = document.querySelectorAll(".main-nav [data-js='with-active']");


	navbarToggler.addEventListener("click", () => {
		mainMenuEls.forEach(item => {
			item.classList.toggle("is-active");
		})
	});
}


/*----------------------------------
Menu & scroll
----------------------------------*/
// const mainMenu = document.querySelector("nav.main-nav");

/*----------------------------------
HeartBeat
----------------------------------*/
function heartBeat(time = 5) {
	return  setInterval(()=>{
		animateCSS("i.mdi-heart", "heartBeat")
	}, time * 1000);
}

/*----------------------------------
Animation effects
 - we can change it from jquery on this https://www.npmjs.com/package/in-view
----------------------------------*/
$("[data-anim]").addClass('hidden animated').each(function () {

	var anim   = $(this).attr('data-anim'),
			offset = $(this).attr('data-offset') ? $(this).attr('data-offset') : 0;

	$(this).viewportChecker({
		classToAdd: anim, // Class to add to the elements when they are visible,
		classToRemove: 'hidden', // Class to remove before adding 'classToAdd' to the elements
		offset: offset, // The offset of the elements (let them appear earlier or later). This can also be percentage based by adding a '%' at the end
		invertBottomOffset: true, // Add the offset as a negative number to the element's bottom
		repeat: false
	});
});

/*----------------------------------
Change menu bg color
----------------------------------*/
function MenuBg() {
	if (window.innerWidth < 768) return;

	let el = document.querySelector("nav.main-nav"),
			className = "darkBg";

	document.addEventListener("scroll", function (ev) {
		let isExists = el.classList.contains(className);
		if(window.pageYOffset > 90){
			if (!isExists){ el.classList.add(className); }
		}else{
			if (isExists){ el.classList.remove(className); }
		}
	})
}


/*----------------------------------
Scroll to
----------------------------------*/
function scrollTo() {
	document.querySelectorAll("[data-scroll]").forEach(el => {
		let toEl = el.getAttribute("href");

		el.addEventListener("click", evt => {
			evt.preventDefault()
			scrollToElement(toEl, {
				offset: -50,
				// ease: 'out-bounce',
				duration: 600
			})
		})

	})
}

/*----------------------------------
INIT
----------------------------------*/
document.addEventListener("DOMContentLoaded", ()=>{
	$("#loading-component").fadeOut("fast");
})

getLastInFolder();
MenuToggler();
heartBeat();
MenuBg();
scrollTo();
disqus();


