import axios from "axios";

const api_url = "https://upfiler.sht-server.cf/api/";
const download_url = "https://upfiler.sht-server.cf/#/download/?t=file&h=";

const aXios = axios.create({
  baseURL: api_url,
  // timeout: 1000,
  headers: {
    'Content-Type': 'multipart/form-data'
  }
});

/**
 * Fetch info about last version from API
 */
function getLastInFolder() {

  let forPost = new FormData();
  forPost.append("action", "file_get_last_in_album");
  forPost.append("album_hash", "08cbdbd1238d321");

  aXios.post("/", forPost).then(({data}) => {
    insertLinksAndName(data)
  }).catch(err => {
    console.log(err);
    return err
  });

}

/**
 * Insert download link and filename into download buttons
 * @param data - fetched info from API
 */
function insertLinksAndName({data}) {

  let links = document.querySelectorAll("[data-download-link]");
  links.forEach(item => {
    item.href = download_url + data.hash;
    item.title = "Download: " +data.filename_orig;

    if(item.hasAttribute("data-download-name")){
      item.innerText = data.filename_orig;
    }
  })
}

export default getLastInFolder;
